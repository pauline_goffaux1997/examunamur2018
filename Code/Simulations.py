""" P. GOFFAUX A. GEVART BACH3 UNamur
#
#
#   1 Day simulation
#   Regular customers : 1000
#       1/3 are Hipsters (budget = 500)
#       2/3 are Regular  (budget = 250)
#   Occasional customers
#       1/10 are Tripadvisor
#       9/10 are Tourist
#
#   At every timestamp
#       20% from the 1000 Regular customers
#           33% (  6.66% Hispters )
#           66% ( 13.33% Regular )
#       80% from Occasional
#           10% ( 8% ) Tripadvisor (with tip)
#           90% (72% ) Tourist
#
#   Regular customers cannot be selected if their expenses (+max drink/food prises) > budget
#       (canConsumme() method evaluate this authorization
#
#   Price for foods and drinks are given
#
#   create set of 1000 customers (regular)
#
#   5 Years simulation : add to a new data frame
#   for year in range( 2013,2017)
#       for days in range (1, 365)
#           for hours in distribution
#               customer = select a customer based on rules
#               customer.order( with hours statistics )
#
#   Compare result with initial data
#       compute average income during the day
#
#   Add select_regular to class CustSet for additional questions
#
    V1.0
        create class CustSet as customers set
            init : create list of regular customers ( 33% Hispters, 66% Regular)
            select : select random customers based on rules (20%, 80%)
            can_order : determin if budget greater than expenses + max prices

    v2.0
        plot bar
            per hours plot_hours()
            per years plot_years
            plot_bar() : plot 1 bar / serie
        plot comparison between CSV and Simulation per years
        plot_years_compare
        plot_compare : plot 2 bars / series

    V3.0
    add Select_regular

    simulate()  modify to handle 2 additional parameters for part 4 scenarii simulation
        simulate( cust_nbr, (inc, year), hip_budget)
    V3.1 correct the customers selection
    V3.2 add proba parameters

    V4.0
"""

from Customer import *
from Exploratory import *
import pandas as pd
import numpy as np
import random



class CustSet(object):
    def __init__(self, size, hipster_budget=500, returning_proba=0.2):
        self.reg_list = []
        self.occ_list = []
        self.reg_max = size
        self.ret_proba = returning_proba  # add ret_prob
        for i in range(0, self.reg_max):
            r = random.random()
            if r < 1 / 3:
                self.reg_list.append(Hipster('Hipster', hipster_budget))
            else:
                self.reg_list.append(Regular('Regular'))

    def select(self):
        # select a customer
        # random between Occasional and Regular
        if random.random() < self.ret_proba: # add parameter ( 0.2 -> returning proba):
            # Regular : select r < reg_max
            r = random.randrange(self.reg_max)
            # return the first with enough budget
            #V3.1. delete selection of the first customer with budget
            return self.reg_list[r]  # firstcustomers with budget
        else:  # select Occasional random Tripadvisor or Tourist
            if random.random() < 0.1:
                self.occ_list.append(Tripadvisor("Tripadvisor"))
                return self.occ_list[-1]  # return new/last elem
            else:
                self.occ_list.append(Tourist("Tourist"))
                return self.occ_list[-1]  # return new/last elem


    def select_regular(self):
        # select a regular customer and return the selection
        r = random.randint(0, self.reg_max-1)
        return self.reg_list[r]

#   Plot average amount over hours
def plot_hours(title, df):
    hours_avg = df.groupby('hours')['amount'].mean()
    x_value = hours_avg.index.tolist()
    y_value = hours_avg.values.tolist()

    plot_bar(title, 'Hours','Average Amount', x_value, y_value)

#   total amount over years
def plot_years(title, df):
    year_tot = df.groupby('years')['amount'].sum()
    x_value = year_tot.index.tolist()
    y_value = year_tot.values.tolist()
    plot_bar(title, 'Years','Total Amount', x_value, y_value)

#   plot single bar
def plot_bar(title, x_label, y_label, x_value, y_value):
    width = 0.45
    rd = range(len(x_value))
    plt.legend([''])
    plt.title(title)
    plt.xlabel(x_label)

    # Create x value
    i = 0
    x_val = []
    step = 1
    if len(x_value) > 15:
        step = len(x_value) // 15

    for x in x_value:
        if i % step == 0:
            x_val.append(x)
        else:
            x_val.append('')
        i += 1

    plt.xticks([r for r in range(len(x_val))], x_val, rotation='vertical')
    #   Y Axis
    plt.ylabel(y_label)
    #
    plt.bar(rd, y_value, width=width, color="blue", edgecolor='blue', linewidth=1)
    plt.show()

#   plot comparison between 2 DF
def plot_years_compare(title, df1, df2):

    df1_g = df1.groupby('years')['amount'].sum()
    df2_g = df2.groupby('years')['amount'].sum()
    x_value = df1_g.index.tolist()
    y1_value = df1_g.values.tolist()
    y2_value = df2_g.values.tolist()
    plot_compare(title, "Years", x_value, "Total Amount", y1_value, y2_value)

#   plot 2 bars
def plot_compare(title, x_legend, x_value, y_legend, y1_value, y2_value):

    width = 0.35
    pos = range(len(x_value))
    plt.legend([""])
    plt.title(title)
    plt.xlabel(x_legend)
    i=0
    x_val = []
    step =  1
    if len(x_value)>15:
        step = len(x_value)//15

    for x in x_value:
        if i % step == 0:
            x_val.append(x)
        else:
            x_val.append('')
        i += 1

    plt.xticks([r + width/2 for r in range(len(x_val))], x_val, rotation='vertical')
    #   Y Axis
    plt.ylabel(y_legend)
    plt.ylim([0, 500000])
    #
    p0 = plt.bar(pos, y1_value, width=width, color="blue", edgecolor='blue', linewidth=1)
    p1 = plt.bar([p + width for p in pos], y2_value, width=width, color="green", edgecolor='green', linewidth=1)
    plt.legend((p0[0], p1[1]), ('CSV', 'Simulation'), loc='upper left')

    plt.show()


# simulation of customers order over 5 years
def simulate(cust_nbr, increase=[], hipster_budget=500, ret_proba=0.2):

    # create customers lists
    customers = CustSet(cust_nbr,hipster_budget,ret_proba)

    # get distribution, drinks and food lists from CSV file
    ghours, drinks_list, food_list = get_proba()

    # simulation parameters days per years
    years_list = [[2013,365], [2014,365], [2015, 365], [2016, 366], [2017, 365]]
    inc_perc, inc_year = 0, 0
    if len(increase) == 2:
        inc_perc = increase[0]
        inc_year = increase[1]

    # run simulation
    simulation=[]
    for year, days, in years_list:
        if inc_perc > 0 and year == inc_year:
            fact = 1 + inc_perc
            print(Customer.drinks_price)
            # Price increase the year
            for k in Customer.drinks_price.keys():
                Customer.drinks_price[k] *= fact
            for k in Customer.food_price.keys():
                Customer.food_price[k] *= fact
            print(Customer.drinks_price)

        for d in range(0, days):
            # iteration on timestamp in a day
            for index, r in ghours.iterrows():
                # get distribution from df
                hour = index
                rec_dist = r.tolist()
                food_dist = rec_dist[6:]  # take from position 6 to the end
                drinks_dist = rec_dist[0:6]
                # select a customer
                c = customers.select()
                if c.can_order():
                    o = c.order(drinks_list, drinks_dist, food_list, food_dist)
                    o.append(c.id)
                    o.append(c.type)
                    o.append(year)
                    o.append(hour)
                    simulation.append(o)

    # format simulation data frame
    # Columns name
    simulation_col = ['DRINKS', 'FOOD', 'tip', 'amount', 'CUSTOMER', 'TYPE', 'years', 'hours']
    # Change list into dictionary with columns name
    dict_simulation = [dict(zip(simulation_col, s)) for s in simulation]
    # Create dataframe
    df_simulation = pd.DataFrame(dict_simulation)

    #function returns 2 values ( a data frame and the customers set
    return df_simulation, customers



#  Main program part 3 : Simulation

if __name__ == "__main__":

    print('simulation')
    df_simulation, customers = simulate(1000)

    df = read_file()
    #   Add amount to the CSV dataframe
    df['amount'] = [Customer.drinks_price.get(d) for d in df['DRINKS']]
    df['amount'] += [Customer.food_price.get(f) for f in df['FOOD']]

    plot_hours("Simulation Average income over days", df_simulation)
    plot_hours("CSV Data  Average income over days", df)
    plot_years("Simulation Income per Years", df_simulation)
    plot_years("CSV Data  Income per Years", df)

    # comparison plot
    plot_years_compare("Income comparison per Years", df, df_simulation)
    #    plot_years("CSV Data  Income per Years", df)

    q1_results(df, 'Data from CSV')
    q1_results(df_simulation, 'Data from Simulation')

    exit(1)
