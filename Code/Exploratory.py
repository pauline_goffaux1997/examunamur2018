# P. GOFFAUX A. GEVART
#
# Exploratory.py v 0.0
#    1) Food and drinks list sold in the coffee bar ?
#       How many unique customers did the bar have ?
#    2) Plot total amount of food and drinks per years ?
#    3) Determine the average sold ( food & drinks ) at any given Time ?

#    Ouverture fichier data
#    Calcul statitics
#    sortie résultats

# V 1.0
# cleaning
# apply naming
# format Q2 with plot 1 &2
# format Q3

#V1.1
#format Q3

#V1.2
#format Q3 output (titles + values)

#V2.0
# correct food and drinks graphs, bugs: names sequence does not equal value sequence
# filter on food <> NA
# get_proba returns 3 values
#   ghours DataFrame
#   drinks_list
#   food_list
# add parameter 'title' to
#   q1_result
#   q2_result
#   q3_result

#V2.1
#correction bugs
#finilization

#V3.0
#tittle adjustement and additional comments
# last bug

#v3.1
#Test if file
#exists

#V4.0
import pandas as pd
import matplotlib.pyplot as plt
import os.path


def read_file():

    # file path and name
    file_path = "../Data"
    file_name = "Coffeebar_2013-2017.csv"
    file1 = file_path + "/" + file_name

    file_path = "Data"
    file2 = file_path + "/" + file_name
    if os.path.isfile(file1):
        df = pd.read_csv(file1, sep=';', na_values=['-'])  # , names=['year', 'cust', 'drinks', 'foods'])
    elif os.path.isfile(file2):
        df = pd.read_csv(file2, sep=';', na_values=['-'])  # , names=['year', 'cust', 'drinks', 'foods'])
    else:
        print('No files ', file1, ' or ', file2, ' found')
        exit(1)


#  Add years and hours to dataframe
#  2013-01-01 08:00:00 --> format exemple
    df['years'] = [t[0:4] for t in df['TIME']]
    df['hours'] = [t[11:16] for t in df['TIME']]
    df.FOOD=df.FOOD.fillna('no food')
    return df

#   get_proba,
#   read CSV file, returns data frame by hours for probability of drinks and food
#   v2.0 add 2 return value : drinks and food list

def get_proba():

    df=read_file()
    #  Get drinks crosstab (tanagra), normalize provide a percentage/distribution
    gdr = pd.crosstab(index=df['hours'], columns=df['DRINKS'], normalize='index')
    gdr_col = [c for c in gdr]


    gfo = pd.crosstab(index=df['hours'], columns=df['FOOD'], normalize='index')
    # change col sequence, push 'no food' at the end
    gfo_col = [col for col in gfo if col != 'no food'] + ['no food']
    #print(gfoCol)
    gfo = gfo[gfo_col]


    # join drinks and food on hours
    ghours = pd.concat([gdr, gfo], axis=1)
    print(ghours)
    # return 3 values DataFrame, drinks list and food list
    return ghours, gdr_col, gfo_col




def q1_results(df, title):
#   Display Q1 Results

    print("Results Q1 ", title)
    print("Q1.1 Drinks list:", df['DRINKS'].unique())
    print("Q1.2 Food list:", [f for f in df['FOOD'].dropna().unique() if f !='no food'])
    print("Q1.3 Customers unique:", len(df['CUSTOMER'].unique()))
    print("Q1.4 Total number of orders :", len(df['CUSTOMER']))

def q2_results(df, title=""):

    # Plot 2 graphs ( volume Drinks and Food over 5 years)
    # Y Axis (Drinks & Food)

    width = 0.45
    plt.ylabel(" Volume ")
    plt.xlabel(" Drinks over 5 Years")

    # Plot 1 : Drinks

    dr = df.groupby(['DRINKS']).size()
    dr_name = dr.index.tolist()
    dr_cnt = dr.values.tolist()

    print("Q2 Drinks List :", dr_name)
    print('Drinks counts :', dr_cnt)
    rd = range(len(dr_name))
    plt.legend(["Plot1 : Drinks"])
    plt.title(title +  'Volume of drinks' )
    plt.xlabel("List of Drinks ")
    plt.xticks([r for r in range(len(dr_name))], dr_name)
    #   Y Axis
    plt.ylabel("Total sales per drinks type")
    #   add Drinks in blue
    plt.bar(rd, dr_cnt, width=width, color="blue", edgecolor='blue', linewidth=2)
    plt.show()

    # Plot 2 : Food, filter on non na ( exclude 'no food')

    food = df[df['FOOD'] != 'no food'].groupby(['FOOD']).size()
    food_name = food.index.tolist()
    food_cnt = food.values.tolist()
    print("Q2 Food List", food_name)
    print('   Food counts', food_cnt)
    rf = range(len(food))
    plt.legend(["Plot 2 : Food"])
    plt.title(title + " Volume of Food ")

    plt.xlabel(" List of Food")
    plt.xticks([r for r in range(len(food_name))], food_name)
    #   Y Axis
    plt.ylabel("Total sales per Food")

    #   add food value in blue
    plt.bar(rf, food, width=width, color="blue", edgecolor='blue', linewidth=2)
    plt.show()



def q3_results(df, title=""):

    # Q3  Average Drinks and food per hours of the day over 5 years
    ghours, drinks_list, food_list = get_proba()
    g_col = [c for c in ghours]
    print('Q3',title)



    # First output format


    for index, rall in ghours.iterrows():
        print("On average, at ",index, " customers drink and eat :")
        #titles drinks & food
        for c in g_col:
            print(c, end=' ')

        #Values
        list = rall.tolist()
        for r in list:
            print("{:3.0%}".format(r), end=' ')
        print('')

    # second output format

    for index, rall in ghours.iterrows():
        print("On average, at ", index, "customers drinks and eat :")
        t = 0
        for r in rall:
                print(g_col[t],end=':')
                print("{:3.0%}".format(r), end=',')
                t += 1
                v=','
        print()

"""
Main program
    readfile
    Q1 : Analyze data (drinks and food names)
    Q2 : plot 1 drinks and plot 2 food
    Q3 : hours distribution
"""

if __name__=="__main__":
    # Main program
    #  Load dta into dataframe
    df=read_file()

    #   Q1 answers ( Drinks and food list )
    q1_results(df, 'Data from CSV')
    #  Q2 answers ( graph .. )
    #hist()
    q2_results(df, 'Data from CSV')
    #   Q3 answeres ( hours statistcics of consumption )
    q3_results(df, 'Data from CSV')


    print(" Fin ")


