"""
    Additional questions
    1) show regular customers buys
        select some customers and display buys
    2) How many returning customers in the CSV ?
        specific time ?
        probability at a specific time of a returning
        correlation between returning and one-timers
    3) Impact to simulate 50 returning customers ?
    4) Impact on price increase in 2015 by 20%
    5) Hipsters budget drop to 40€
    6) ...
    V1.0
        plot_bar
        print_buys_hist : print the buys of a customer
        csv_regular : identify regular customers in the CSV data
    # syntax error : simulations

    V2.0
    Add seasonal analysis (4.6)
    V4.0
"""
# from Customer import *
from Exploratory import *
from Simulations import *


def plot_single_bar(title, y_title, x_value, y_val):

    ind = np.arange(len(x_value))  # the x locations for the groups
    width = 0.35  # the width of the bars: can also be len(x) sequence
    y = np.array((y_val))

    p = plt.bar(ind, y, width, color='#d62728')
    plt.ylabel(y_title)
    plt.title(title)
    plt.xticks(ind, x_value)
    i=0
    x_val = []
    step =  1
    if len(x_value)>15:
        step = len(x_value)//15

    for x in x_value:
        if i % step == 0:
            x_val.append(x)
        else:
            x_val.append('')
        i += 1

    plt.xticks([r for r in range(len(x_val))], x_val, rotation='vertical')
    #plt.legend((p[0], l))

    plt.show()


def plot_bar(title, y_title, x_value, l, y1, y2 ):

    #hours_avg = df.groupby(['TYPE', 'year'])['amount'].sum()

    #x_value = hours_avg.index.tolist()
    #y_value = hours_avg.values.tolist()
    N = 5
    # Take data from data frame
    hips_means = np.array((20, 35, 30, 35, 27))
    regu_means = np.array((25, 32, 70, 50, 125))
    trip_means = np.array((150, 210, 350, 200, 400))
    tour_means = np.array((13, 20, 35, 20, 40))
    ind = np.arange(len(x_value))  # the x locations for the groups
    width = 0.35  # the width of the bars: can also be len(x) sequence
    t = np.array((y1))
    r = np.array((y2))
    p1 = plt.bar(ind, t, width, color='#d62728')
    p2 = plt.bar(ind, r, width, bottom=t)
#    p3 = plt.bar(ind, trip_means, width, bottom=y1+y2)
#    p4 = plt.bar(ind, tour_means, width, bottom=y1+y2+trip_means)

    plt.ylabel(y_title)
    plt.title(title)
    plt.xticks(ind, x_value)
    i=0
    x_val = []
    step = 1
    if len(x_value)>15:
        step = len(x_value)//15

    for x in x_value:
        if i % step == 0:
            x_val.append(x)
        else:
            x_val.append('')
        i += 1

    plt.xticks([r for r in range(len(x_val))], x_val, rotation='vertical')

#    plt.yticks(np.arange(0, 1000, 100))
    plt.legend((p1[0], p2[0]), l)

    plt.show()


#   prints buying histories
def print_buys_hist(df, customers, nbr):
    print('Part 4 : Customers Buying Histories for ', nbr, " customers")
    # print buying histories of customers
    for c in range(0,nbr):
        cust = customers.select_regular()
        # select customers buys from df
        df_sel = df[df['CUSTOMER'] == cust.get_id()]
        print("customer", cust.get_id(), ' historical buys from Customer')
        cust.print()
        print("Customer:",cust.get_id()," historical buys from Dataframe:")
        print(df_sel)
        title = "Customer : "+cust.get_id()+ " historical buys"
        plot_hours(title, df_sel)
        plot_years(title, df_sel)


def csv_regular(df):

    # set regular value to duplicate customers
    df['REG'] = df['CUSTOMER'].duplicated(keep=False)
    reg_nbr = len(df['CUSTOMER'][df['REG']].unique())
    tour_nbr = len(df['CUSTOMER'][df['REG'] == False].unique())
    cust_nbr = len(df['CUSTOMER'].unique())
    print('Returning customers from CSV')
    print('Returning   :', reg_nbr)
    print('Tourists  :', tour_nbr)
    print('Total     :', cust_nbr)


    # Comparison Tourist / Regular per hours
    df_reg = df[df['REG']]
    # Normalized data
    g_hours = pd.crosstab(index=df['hours'], columns=df['REG'], normalize='index')
    g_hours.columns = ['Tourist','Regular']
    print(g_hours)
    x = g_hours.index.tolist()
    y1 = g_hours['Tourist'].values.tolist()
    y2 = g_hours['Regular'].values.tolist()
    l = ("Tourist", 'Regular')
    t = ('% Repartition Tourist and Regular from CSV')
    y_title = ('% by customer type')
    plot_bar(t, y_title, x, l, y1, y2)

    # Amount per customer type over the day
    g_hours = pd.crosstab(index=df['hours'], columns=df['REG'], values=df.amount, aggfunc=sum)
    g_hours.columns = ['Tourist', 'Regular']
    print(g_hours)
    x = g_hours.index.tolist()
    y1 = g_hours['Tourist'].values.tolist()
    y2 = g_hours['Regular'].values.tolist()
    l = ("Tourist", 'Regular')
    y_title =('Amount per customer type' )
    t = ('Amount Repartition Tourist and Regular from CSV')
    plot_bar(t, y_title, x, l, y1, y2)


def seasonal(df):
    df['months'] = [t[5:7] for t in df['TIME']]
    plot_months('Seasonal analysis per month over 5 Years', df)


def plot_months(title, df):
    m_sum = df.groupby('months')['amount'].sum()
    x_value = m_sum.index.tolist()
    y_value = m_sum.values.tolist()
    plot_single_bar(title, 'Total Amount', x_value, y_value)


# additional questions
#   Execute simulation with parameters
#   display results

if __name__ == "__main__":

    drinks_price = {"milkshake": 5, "frappucino": 4, "water": 2, "soda": 3, "tea": 3, "coffee": 1}
    food_price = {"sandwich": 5, "cookie": 2, "muffin": 3, "pie": 3, "no food": 0}

    prices = {}
    # simulate
    df_simulation, customers = simulate(1000)
    # Part 4.1 print customers buys
    print_buys_hist(df_simulation, customers, 1)

    # Part 4.2 Identify regular customers from csv file
    print('Part 4.2 : print nbr of Regular from CSV')
    df = read_file()
    # Add amount to df
    df['amount'] = [drinks_price.get(d) for d in df['DRINKS']]
    df['amount'] += [food_price.get(f) for f in df['FOOD']]

    csv_regular(df)

    # Part 4.3 SImulat 50 Customers and compare
    # simulate 50 Regular Customers
    print("Part 4.3 Simulate 50 Customers, compare results")
    df_simulation, customers = simulate(50)
    plot_years_compare("Income comparison 50 customers Years", df, df_simulation)

    # simulate 1000 customers price increase by 20% in 2015
    print("Part 4.4 Simulate prices increase 2015, compare results")
    df_simulation, customers = simulate(1000, [0.2, 2015])
    plot_years_compare("Income comparison 20% increase", df, df_simulation)

    # Simulate 1000 customers, Hipsters budget from 500 to 40
    print("Part 4.5 Simulate Hipsters budget : 40€, compare results")
    df_simulation, customers = simulate(1000, (), 40)
    plot_years_compare("Income comparison Hipster budget 40€", df, df_simulation)

    # Additional ...
    print("Part 4.6 simulation 40% distribution retruning")
    df_simulation, customers = simulate(1000, (), 500, 0.4)
    plot_years_compare("Income Simulation of 40% returning Customers", df, df_simulation)

    df_reg = df_simulation[df_simulation['TYPE'].isin(['Regular', 'Hipster'])]
    plot_years("Simulation Regular and Hipster", df_reg)
    plot_years("Simulation all customers", df_simulation)


