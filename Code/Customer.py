# P. GOFFAUX A. GEVART  Bach 3 Ingetech UNamur
#
# customers.py V0.0
# Create object classes
#                          Customer
#                            !
#              +-------------+-------+
#              !                     !
#          Occasional              Returning
#               !                       !
#           +---+---+               +---+-----+
#           !       !               !         !
#      Tourist   TripAdvisor    Regular   Hipster
#
#
#v1.0
#Test classes
# Cleaning
#Apply naming

#V1.1
#V1.2
#add print customer
#get back order

#2.0
#Modify Hispters __init__
#Add parameter to set Hipster budget to 40

#V2.1
# changes in nemes : get_budget and get_id
#V4.0

from random import randint, random
from Exploratory import get_proba
import random

# Class Customer

class Customer(object):
    # Class properties
    _gblId = 0 # objects counter
    # drink prices and food prices
    drinks_price = {"milkshake":5, "frappucino": 4, "water": 2, "soda": 3, "tea":3, "coffee": 3}
    food_price = {"sandwich":5, "cookie": 2,"muffin":3,"pie":3, "no food":0 }

    def __init__(self, type):
        Customer._gblId += 1
        # Objects properties
        self.type = type
        self.id = 'CUST{0:=06}'.format(Customer._gblId)  # format "CUST000001"
        self.drinks = []
        self.foods = []
        self.tips = []
        self.amounts = []
        self.budget = 100
        self.expenses = 0

    def get_budget(self):
        return self.budget

    def get_id(self):
        return self.id

    def print(self):
        print()
        print('Id:',self.id, 'Type:', self.type, 'Budget:', self.budget, 'Expenses:', self.expenses, 'tip:', self.tip())
        print('Drinks:', len(self.drinks))
        print([d for d in self.drinks])

        # print food with filter in 'no food'
        print('Foods:', sum([1 for f in self.foods if f != 'no food']))
        print([f for f in self.foods if f != 'no food'])

    def tip(self):
        return 0

    #  selection randomly on a list with frequency
    def select(self, clist):
        r = random.random()
        s = 0
        freq_cumul = 0
        for f in clist:
            freq_cumul += f
            if freq_cumul <= r:
               s += 1 # cumul Freq less than r, continue to evaluate range
            else:
                break # freq cumul above r, return the index
        return s

    # execute an order
    def order(self, drinks_list, drinks_dist, food_list, food_dist):
        order=[]
        amount = 0

        # select a drink
        d = self.select(drinks_dist)
        self.drinks.append(drinks_list[d])
        order.append(drinks_list[d])
        amount += self.drinks_price.get(drinks_list[d])

        # select a food
        f = self.select(food_dist)
        self.foods.append(food_list[f])
        order.append(food_list[f])
        amount += self.food_price.get(food_list[f])

        # add a tip
        tip = self.tip()
        self.tips.append(tip)
        order.append(tip)
        amount += tip
        order.append(amount)

        self.amounts.append(amount)

        # Add amount to the customers expenses account

        self.expenses += amount
        return order


    # determine authorization to consume
    def can_order(self):
        #  compute if a customer can consume and return True or False
        #  customers can consume if the expense + max food and drink price are lower or equal to the budget
        return self.expenses + max([i for i in self.drinks_price.values()]) + max([i for i in self.food_price.values()]) <= self.budget

# Class Occasional

class Occasional(Customer):

    def __init__(self, id):
        Customer.__init__(self, id)
        self.budget = 100

    def can_order(self):
        # The once customers can buy only one time ( if len = 0 )
        return (len(self.drinks) == 0)

# Class Returning

class Returning(Customer):
    def __init__(self, id):
        Customer.__init__(self,id)
        self.budget = 250

# Class Tourist

class Tourist(Occasional):

    def __init__(self, id):
        Occasional.__init__(self, id)

# Class Tripadvisor

class Tripadvisor(Occasional):
        def __init__(self, id):
            Occasional.__init__(self, id)


        def tip(self):
            return randint(1,10)

# Class Regular

class Regular(Returning):
    def __init__(self, id):
        Returning.__init__(self, id)
        self.budget = 250

# Class Hipster
# V2.0 additional parameters to set budget value
class Hipster(Returning):
    # Add parameter init_budget with default value = 500
    def __init__(self, id, init_budget =500):
        Customer.__init__(self, id)
        self.budget = init_budget

"""
Main program
test customers class
test distribution calculation


if __name__ == "__main__":


#  Objets de la hierarchie de classe

    cust = Customer('Pauline')
    occa = Occasional('Raphaëlle')
    tour = Tourist('Louis')
    trip = Tripadvisor("Manon")
    retu = Returning("Léa")
    pres = Returning('Alexandre')
    old_pres = Returning('Gilles')
    regu = Regular("Antoine")
    hips = Hipster("Jonathan")

# list with 7 customers
cust_list = (cust, occa, tour,trip, retu, regu, hips)

# Get data from csv and compute distribution over hours
df, drinks_list, food_list = get_proba()

for index, r in df.iterrows():  # index = hours, r = distribution for drinks and food
    dist = r.tolist()
    food_dist = dist[len(drinks_list):]  # take elem from position 6 to the enddrinks_dist = dist[0:len(drinks_list)]  # take the 6 first elem
    drinks_dist = dist[0:len(drinks_list)]  # take the 6 first elem
    for cust in cust_list:

        # test if a customer has enough budget to buy most expensive drink and food
        if cust.can_order():
            # Customer orders drinks/food with drinks/food distribution
            order = cust.order(drinks_list, drinks_dist, food_list, food_dist)
            print('Customer Id :' , cust.id, " orders:", order)

# Print customer list
for cust in cust_list:
    # print all customer info included their consumption
    cust.print()
"""