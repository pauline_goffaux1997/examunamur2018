# README #

Project of Data Analytics
Pauline Goffaux & Antoine Gévart

How to use the programs :
create project directories
code sub directories with the 4 programs
Exploratory.py
Customers.py
Simulation.py
Additional.py
data directories with the CSV file

Within PyCharm :
run Exploratory to get answers of the part 1
run Customers to test the 7 Customers classes hierarchy
run Simulation to run a first simulation and get comparison
run Additional to have other analysis and compute simulations
